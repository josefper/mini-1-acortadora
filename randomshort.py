#!/usr/bin/python

import socket
from urllib.parse import unquote
import random
import string
import shelve


FORM = """
<html>
    <body>
        <hr>
        <form action="/" method="POST">
          <label for="url">Url:</label>
          <input type="text" id="url" name="url"><br>
        
          <input type="submit" value="Enviar">
        </form>
    </body>
</html>
"""

PAGE = """
<html>
    <body>
        <hr>
        <form action="/" method="POST">
          <label for="url">Url:</label>
          <input type="text" id="url" name="url"><br>
        
          <input type="submit" value="Enviar">
        </form>
        <hr>
        <p>URLs: {list}</p>
    </body>
</html>
"""

ERROR_PAGE = """
<html>
    <body>
    <hr>
        <form action="/" method="POST">
          <label for="url">Url:</label>
          <input type="text" id="url" name="url"><br>
        
          <input type="submit" value="Enviar">
        </form>
        <hr>
        <h1>URL no valida</h1>
    </body>
</html>
"""

REDIRECT_PAGE = """
<html>
    <body>
        <h1>
            Redirecting... 
        </h1>
    </body>
</html>
"""


class WebApp:

    def post(self, parsed_request):
        if parsed_request['url'] not in self.resources and parsed_request['url'] != "":
            self.resources[parsed_request['url']] = "/" + self.generate_random_resource()
        if parsed_request['url'] == "":
            html_page = ERROR_PAGE
            http_code = "404 NOT FOUND"
        else:
            html_page = PAGE.format(list=self.resources)
            http_code = "200 OK"

        return html_page, http_code

    def generate_random_resource(self):
        letters_and_digits = string.ascii_letters + string.digits
        random_string = ''.join(random.choice(letters_and_digits) for i in range(5))
        return random_string

    def redirect(self, parsed_request):
        if parsed_request['resource'] in self.resources.values():
            for key, value in self.resources.items():
                if value == parsed_request['resource']:
                    html_page = REDIRECT_PAGE
                    http_code = "302 Temporal Redirect"
                    redirect_url = key
                    break
                else:
                    html_page = ERROR_PAGE
                    http_code = "404 NOT FOUND"
        else:
            html_page = ERROR_PAGE
            http_code = "404 NOT FOUND"
        return html_page, http_code, redirect_url

    def process(self, parsed_request):
        """Procesa con el recurso de la respuesta"""
        redirect_url = ""
        # Compruebo si es una peticion de tipo GET y si es el recurso principal
        if parsed_request['method'] == 'GET' and parsed_request['resource'] == '/':
            html_page = FORM
            http_code = "200 OK"
        # Compruebo si es una peticion de tipo POST y si es el recurso principal
        elif parsed_request['method'] == 'POST' and parsed_request['resource'] == '/':
            html_page, http_code = self.post(parsed_request)
        # Compruebo si es una peticion de tipo GET y si es un recurso que ya existe
        elif parsed_request['method'] == 'GET' and parsed_request['resource'] != '/':
            html_page, http_code, redirect_url = self.redirect(parsed_request)
        else:
            # Por si me llega un metodo o recurso que no controlo
            html_page = PAGE.format(list="Error 404")
            http_code = "404 NOT FOUND"

        return http_code, html_page, redirect_url

    def parse(self, request):
        """"Quedarme con el tipo de accion, el nombre de recurso y el contenido"""
        data = {}
        body_start = request.find('\r\n\r\n')

        if body_start == -1:
            # No hay body en la peticion
            data['body'] = None
            data['method'] = "GET"
        else:
            # Hay cuerpo, me lo quedo
            data['body'] = request
            try:
                url = data['body'].find('url=')
                if url == -1:
                    data['url'] = ""
                else:
                    data['url'] = unquote(data['body'][url + 4:]) # Se utiliza unquote para mostrar el texto plano
                    if not data['url'].startswith('http://') and not data['url'].startswith('https://') and data[
                        'url'] != "":
                        data['url'] = "https://" + data['url']

                parts = data['body'].split(' ', 2)
                data['method'] = parts[0]
                data['resource'] = parts[1]
            except IndexError:
                data['url'] = ""

        return data

    def save_data(self, diccionario):
        with shelve.open(self.filename) as shelf:
            for key, value in diccionario.items():
                shelf[key] = value

    # Función para cargar datos del archivo shelve
    def load_data(self):
        with shelve.open(self.filename) as shelf:
            return dict(shelf)

    def __init__(self, host, port):

        # Abro mi archivo con las urls
        self.filename = "urls.db"
        self.resources = self.load_data()

        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # AF_INET - IPv4
        # SOCK_STREAM - TCP

        # Voy a arreglar lo del puerto ocupado
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Anclo el socket
        mySocket.bind((host, port))

        # Meto en cola un maximo de 5 conexiones TCP
        mySocket.listen(5)

        while True:
            print("Esperando alguna conexion...")
            connectionSocket, addr = mySocket.accept()
            print("Conexion recibida de: " + str(addr))
            recibido = connectionSocket.recv(2048)
            print(recibido)

            parsed_request = self.parse(recibido.decode('utf-8'))

            # Proceso la peticion y creo la respuesta
            return_code, html_respuesta, redirect_url = self.process(parsed_request)
            self.save_data(self.resources)
            print(return_code)

            # Pagina web
            recurso = "HTTP/1.1 " + return_code + "\r\n\r\n" \
                      + html_respuesta + "\r\n"

            redireccion = "HTTP/1.1 " + return_code + "\r\n" \
                          + "location:" + redirect_url \
                          + "\r\n"

            # Codificar a UTF-8 para poder enviar el mensaje. Se debe elegir entre utilizar recurso o redireccion
            if redirect_url == "":
                connectionSocket.send(recurso.encode('utf-8'))
                connectionSocket.close()
            else:
                connectionSocket.send(redireccion.encode('utf-8'))
                connectionSocket.close()


if __name__ == '__main__':
    myWeb = WebApp('localhost', 1234)
